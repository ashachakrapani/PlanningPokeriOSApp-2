//
//  ViewController.swift
//  PlanningPoker
//
//  Created by Asha Chakrapani on 6/13/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    //MARK: - UIViewController overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "TaskListSegue") {
            if let taskListViewController = segue.destination as? TaskListViewController {
                if let _memberIdString = self.memberIdTextField.text {
                    self.memberId = Int(_memberIdString)
                }
                taskListViewController.memberId = self.memberId
            }
        }
    }
    
    //MARK: - IBOutlets

    @IBOutlet weak var memberIdTextField: UITextField!
    @IBOutlet weak var viewAllTasksButton: UIButton!
    
    //MARK: - IBActions
    
    @IBAction func viewAllTasksButtonTouchUp(_ sender: Any) {
        if (self.memberIdTextField.text?.isEmpty ?? false || self.memberIdTextField.text == nil) {
            self.presentAlertViewController(title: "Member Id Required", message: "Enter member id to proceed.")
        }
        self.performSegue(withIdentifier: "TaskListSegue", sender: self)
    }
    
    //MARK: - Private API
    private var memberId: Int?
    
}

