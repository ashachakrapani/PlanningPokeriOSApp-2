//
//  TaskDetailViewController.swift
//  PlanningPoker
//
//  Created by Asha Chakrapani on 6/16/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import UIKit

struct PointsSummary {
    let point: [FibonacciLevel]?
    let pointsCount: [Int]?
}

class TaskDetailViewController: UIViewController  {

    //MARK: - UIViewController overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configurePickerView()
        self.setUpNavigationBar()
        self.fetchDashboardData()
        self.taskController.listenToTaskPointUpdatedEvent{ (updatedTask) in            
            self.fetchDashboardData()
        }
    }
    
    //MARK: - IBOutlets
    @IBOutlet weak var taskTitleLabel: UILabel!
    @IBOutlet weak var taskIdLabel: UILabel!
    @IBOutlet weak var taskDescriptionLabel : UILabel!
    @IBOutlet weak var pointsTextField: UITextField!
    @IBOutlet weak var pointsCountLabelStackView: UIStackView!
    @IBOutlet weak var pointLabelStackView: UIStackView!
    @IBOutlet weak var pointsSummaryTitle: UILabel!
    
    //MARK: - Public API
    var selectedTask: TaskViewModel?
    var memberId: Int?
    var taskSummaryViewModel: PointsDashboardResponse?
    
    //MARK: - Private API
    private lazy var pickerView = PickerView(frame: CGRect(x: 0, y: 100, width: self.view.frame.size.width, height: 200))
    private lazy var taskController = TaskController()
    
    
    private func configurePickerView() {
        self.pointsTextField.inputView = self.pickerView
        self.pickerView.delegate = self
        self.pickerView.responderInputView = self.pointsTextField
    }
    
    private func setUpNavigationBar() {
        let shareTaskNavBarButton = UIBarButtonItem(barButtonSystemItem: .action , target: self, action: #selector(shareTaskButtonTouchUp(sender:)))
        self.navigationItem.rightBarButtonItem = shareTaskNavBarButton
    }
    
    @objc private func shareTaskButtonTouchUp(sender: UIBarButtonItem) {
        guard let taskId = self.selectedTask?.taskId, let taskDescription = self.selectedTask?.taskDescription else { return }
        let activityViewController = UIActivityViewController(
            activityItems: ["Here is the decription of the task I was looking at. Task ID \(taskId) - \(taskDescription)"],
            applicationActivities: nil)
        present(activityViewController, animated: true, completion: nil)
    }
    
    private func fetchDashboardData() {
        if let taskId = self.selectedTask?.taskId {
            self.taskController.fetchPointsDashboardData(forTaskId: taskId) { (response) in
                self.taskSummaryViewModel = response
                self.updateViewFromViewModel()
            }
        }
    }
    
    private func updateViewFromViewModel() {
        let _selectedTaskViewModel = self.selectedTask
        self.taskTitleLabel.text = _selectedTaskViewModel?.taskTitle ?? ""
        if let taskId = _selectedTaskViewModel?.taskId {
            self.taskIdLabel.text = String(taskId)
        } else {
            self.taskIdLabel.text = ""
        }
        if let taskPoints = _selectedTaskViewModel?.taskPoint {
            self.pointsTextField.text = String(taskPoints)
        } else {
            self.pointsTextField.text = ""
        }
        self.taskDescriptionLabel.text = _selectedTaskViewModel?.taskDescription ?? ""
        self.pointsSummaryTitle.text = NSLocalizedString("Points Summary", comment: "Title for points summary section")
        self.clearDashboardData()
        self.updatedDashboardData()
    }
    
    struct DashboardData {
        let point: Int
        var count: Int
    }
    
    private func clearDashboardData() {
        for pointView in self.pointLabelStackView.arrangedSubviews {
            self.pointLabelStackView.removeArrangedSubview(pointView)
        }
        for countView in self.pointsCountLabelStackView.arrangedSubviews {
            self.pointsCountLabelStackView.removeArrangedSubview(countView)
        }
    }
    
    private func updatedDashboardData() {
        self.clearDashboardData()
        var dashboardDataArray: [DashboardData] = []
        if let _viewModel = self.taskSummaryViewModel, let taskSummaryArray = _viewModel.items {
            for taskSummary in taskSummaryArray {
                if let taskPoint = taskSummary.taskPoint {
                    if let indexOfCount = dashboardDataArray.index(where: {$0.point == taskPoint}) {
                        dashboardDataArray[indexOfCount].count += 1
                    } else {
                        dashboardDataArray.append(DashboardData(point: taskPoint, count: 1))
                    }
                }
            }
        }
        
        for data in dashboardDataArray {
            let pointLabel = UILabel()
            let pointCountLabel = UILabel()
            pointLabel.text = String(data.point)
            pointCountLabel.text = String(data.count)
            self.pointLabelStackView.addArrangedSubview(pointLabel)
            self.pointsCountLabelStackView.addArrangedSubview(pointCountLabel)
        }
    }
}

//MARK: TaskDetailViewController extensions

extension TaskDetailViewController: PickerViewDelegate {
    func pickerViewItemSelected(selectedItem item: FibonacciLevel) {
//        if (self.selectedTask?.taskSummary == nil) {
//            self.selectedTask?.taskSummary = []
//        }
//        var updatedTaskSummary: TaskPoints? = nil
//        if let memberIds = self.selectedTask?.taskSummary?.compactMap({ $0.memberId }), let currentMemberId = self.memberId{
//            if let index = memberIds.index(of: currentMemberId) {
//                self.selectedTask?.taskSummary?[index].taskPoint = item.rawValue
//                updatedTaskSummary = self.selectedTask?.taskSummary?[index]
//            } else {
//                updatedTaskSummary = TaskPoints(taskPoint: item.rawValue, memberId: self.memberId)
//                self.selectedTask?.taskSummary?.append(TaskPoints(taskPoint: item.rawValue, memberId: self.memberId))
//            }
//        }
        
//        if let objectId = self.sel ectedTask?.objectId, let taskId = self.selectedTask?.taskId, let _updatedTaskSummary = updatedTaskSummary {
//            self.taskController.emitTaskPointUpdateEvent(objectId: objectId, forTaskId: taskId, updatedTaskSummary: _updatedTaskSummary) { (taskUpdateResponse) in
//                self.updateTaskDetailsFromViewModel()
//            }
//        }
        self.selectedTask?.taskPoint = item.rawValue
        self.selectedTask?.memberId = self.memberId

        if let objectId = self.selectedTask?.objectId, let taskId = self.selectedTask?.taskId, let taskPoint = self.selectedTask?.taskPoint, let memberId = self.selectedTask?.memberId {
            self.taskController.emitTaskPointUpdateEvent(objectId: objectId, forTaskId: taskId, withPoints: taskPoint, byMemberId: memberId) { (taskUpdateResponse) in
                self.updateViewFromViewModel()
            }
        }
    }
}

//MARK: - TextF

extension TaskDetailViewController: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if (textField == self.pointsTextField) {
            self.pickerView.movePicker(toItem: FibonacciLevel.zero)
        }
        return true
    }
}



