//
//  PickerView.swift
//  PlanningPoker
//
//  Created by Asha Chakrapani on 6/16/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import UIKit

protocol PickerViewDelegate: class{
    func pickerViewItemSelected(selectedItem item: FibonacciLevel)
}
class PickerView: UIView {

    //MARK: - IBOutlets
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var doneButtonTouchUp: UIButton!
    
    //MARK: - UIView overrides

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    //MARK: - IBActions
    
    @IBAction func doneButtonTouchUp(_ sender: Any) {
        self.responderInputView?.endEditing(true)
    }
    
    //MARK: - Public API
    
    weak var delegate: PickerViewDelegate?
    weak var responderInputView: UIView?
    
    func movePicker(toItem item: FibonacciLevel) {
        if let index = FibonacciLevel.allAvailablePoints.index(of: item) {
            self.pickerView.selectRow(index, inComponent: 0, animated: true)
        }
    }
    
    //MARK: - Private API

    private func commonInit() {
        self.configureNib()
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
    }
    
    private func configureNib() {
        let xibView = Bundle.main.loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)?.first as! UIView
        xibView.frame = self.bounds
        xibView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(xibView)
        self.addConstraints(from: xibView, to: self)
    }
    
    private func addConstraints(from childView: UIView, to parentView: UIView) {
        let bottomConstraint = NSLayoutConstraint(item: parentView, attribute: .bottom, relatedBy: .equal, toItem: childView, attribute: .bottom, multiplier: 1, constant: 0 )
        let topConstraint = NSLayoutConstraint(item: parentView, attribute: .top, relatedBy: .equal, toItem: childView, attribute: .top, multiplier: 1, constant: 0 )
        let rightConstraint = NSLayoutConstraint(item: parentView, attribute: .right, relatedBy: .equal, toItem: childView, attribute: .right, multiplier: 1, constant: 0 )
        let leftConstraint = NSLayoutConstraint(item: parentView, attribute: .left, relatedBy: .equal, toItem: childView, attribute: .left, multiplier: 1, constant: 0 )
        self.addConstraints([topConstraint, bottomConstraint, leftConstraint, rightConstraint])
    }
    
}

//MARK: - PickerView extensions

extension PickerView: UIPickerViewDelegate, UIPickerViewDataSource{ 
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return FibonacciLevel.allAvailablePoints.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(FibonacciLevel.allAvailablePoints[row].rawValue)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let selectedPickerItem = FibonacciLevel.allAvailablePoints[row]
        self.delegate?.pickerViewItemSelected(selectedItem: selectedPickerItem)
    }
}

