//
//  TasksTableViewCell.swift
//  PlanningPoker
//
//  Created by Asha Chakrapani on 6/14/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import UIKit

class TaskTableViewCell: UITableViewCell {

    //MARK: - IBOutlet
    
    @IBOutlet weak var taskTitleLabel: UILabel!
    @IBOutlet weak var taskDescriptionLabel: UILabel!
    
    //MARK: - UITableviewCell Overrides
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Public API
    
    func resetCell() {
        self.taskDescriptionLabel.text = ""
    }

}
