//
//  TaskViewModel.swift
//  PlanningPoker
//
//  Created by Asha Chakrapani on 6/16/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation

public struct TaskViewModel: Codable {
    
    //MARK: - init
    init(withTask task: Task) {
        self.objectId = task.objectId
        self.taskId = task.taskId
        self.taskTitle = task.taskTitle
        self.taskDescription = task.taskDescription
        self.taskPoint = task.points?.rawValue
        self.memberId = nil
    }
    
    ///Gets or sets the object id of the task
    let objectId: String?
    
    ///Gets or sets the task id
    let taskId: Int?
    
    ///Gets or sets the task title
    let taskTitle: String?
    
    ///Gets or sets the task description
    let taskDescription: String?
    
    ///Gets or sets points assigned to the task by the memberId
    var taskPoint: Int?
    
    ///Gets or sets the memberId logged in
    var memberId: Int?
}
