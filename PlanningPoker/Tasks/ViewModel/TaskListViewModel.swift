//
//  TaskViewModel.swift
//  PlanningPoker
//
//  Created by Asha Chakrapani on 6/16/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation

public struct TaskListViewModel: Codable {
    
    init(withTask tasks: [Task]?) {
        self.taskList = TaskListViewModel.fromTasks(tasks)
    }
    
    let taskList: [TaskViewModel]?
    
    private static func fromTasks(_ tasks: [Task]?) -> [TaskViewModel]? {
        guard let _tasks = tasks else { return nil }
        return _tasks.map { TaskViewModel.init(withTask: $0) }
    }
}
