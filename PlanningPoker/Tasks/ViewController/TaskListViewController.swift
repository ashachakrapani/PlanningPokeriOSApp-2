    //
    //  TasksTableViewController.swift
    //  PlanningPoker
    //
    //  Created by Asha Chakrapani on 6/14/18.
    //  Copyright © 2018 Asha Chakrapani. All rights reserved.
    //
    
    import Foundation
    import UIKit
    
    class TaskListViewController: UIViewController {
        
        //MARK: - UIViewController overrides
        
        override func viewDidLoad() {
            super.viewDidLoad()
            self.setUpUI()
            //Make service call to fetch task list here
            self.fetchTasks()
        }
        
        //MARK: - IBOutlets
        
        @IBOutlet weak var tableView: UITableView!
        
        //MARK: - Private API
        
        private var selectedTask: TaskViewModel?
        private var taskListViewModel: TaskListViewModel?
        private lazy var taskController = TaskController()
        
        private func setUpUI() {
            self.tableView.estimatedRowHeight = 75
            self.tableView.rowHeight = UITableViewAutomaticDimension
            //TODO: Hide empty rows in the table
            self.setUpNavigationBar()
        }
        
        private func setUpNavigationBar() {
            let addTaskNavBarButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTaskButtonTouchUp(sender:)))
            self.navigationItem.rightBarButtonItem = addTaskNavBarButton
            self.navigationItem.title = NSLocalizedString("Task List", comment: "Title for task list table")
        }
        
        @objc private func addTaskButtonTouchUp(sender: UIBarButtonItem) {
            let addTaskViewController = UIStoryboard(name: "AddTask", bundle: Bundle.main).instantiateViewController(withIdentifier: "AddTaskViewController") as! AddTaskViewController
            addTaskViewController.delegate = self
            self.navigationController?.pushViewController(addTaskViewController, animated: false)
        }
        
        private func fetchTasks() {
            self.taskController.fetchTasks { (response) in
                if let taskFetchResponse = response {
                    self.taskListViewModel = TaskListViewModel.init(withTask: taskFetchResponse.tasks)
                    self.tableView.reloadData()
                }
            }
        }
        
        private func editTask() {
            self.performSegue(withIdentifier: "TaskDetailSegue", sender: self)
        }
        
        private func deleteTask() {
            guard let taskObjectId = self.selectedTask?.objectId else { return }
            self.taskController.deleteTask(withObjectId: taskObjectId) { (statusCode, deleteTaskResponse) in
                //write a convenience function for the below
                if let _statusCode = statusCode, _statusCode != 200 {
                    self.presentAlertViewController(title: "Error", message: "There was an error deleting the task.")
                } else {
                    //update table data source to reflect the change
                    self.fetchTasks()
                }
            }
        }
        
        //MARK: - Public API
        
        var memberId: Int?
        
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if ( segue.identifier == "TaskDetailSegue") {
                if let taskDetailViewController = segue.destination as? TaskDetailViewController {
                    taskDetailViewController.selectedTask = self.selectedTask
                    taskDetailViewController.memberId = self.memberId
                }
            }
        }
    }
    
    //MARK: - TasksTableViewController extensions
    
    extension TaskListViewController: UITableViewDataSource, UITableViewDelegate {
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.taskListViewModel?.taskList?.count ?? 0
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "TaskTableViewCell", for: indexPath)
            if let taskTableCell = cell as? TaskTableViewCell, let taskListViewModel = self.taskListViewModel {
                taskTableCell.resetCell()
                let taskViewModel = taskListViewModel.taskList?[indexPath.row]
                taskTableCell.taskTitleLabel.text = taskViewModel?.taskTitle
                taskTableCell.taskDescriptionLabel.text = taskViewModel?.taskDescription
            }
            return cell
        }
    
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            if let taskListViewModel = self.taskListViewModel {
                self.selectedTask = taskListViewModel.taskList?[indexPath.row]
                self.selectedTask?.memberId = self.memberId
                self.performSegue(withIdentifier: "TaskDetailSegue", sender: self)
            }
        }
        
        func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
            return true
        }
        
        func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
            guard let taskListViewModel = self.taskListViewModel else { fatalError("Attempt to row actions without a managed object") }
                self.selectedTask = taskListViewModel.taskList?[indexPath.row]
            let editAction: UIContextualAction = UIContextualAction(style: .normal, title: "Edit") { [weak self] (action, view, completionHandler) in
                self?.editTask()
                completionHandler(true)
            }
            
            let deleteAction: UIContextualAction = UIContextualAction(style: .destructive, title: "Delete") { [weak self] (action, view, completionHandler) in
                self?.deleteTask()
                completionHandler(true)
            }
            
            let configuration = UISwipeActionsConfiguration(actions: [editAction, deleteAction])
            configuration.performsFirstActionWithFullSwipe = false
            return configuration
        }
    }
    
    extension TaskListViewController: NewTaskDelegate {
        func save(newTaskViewModel taskViewModel: NewTaskViewModel) {
            //Make network call to save the new task to database
            self.taskController.createNewTask(withDetails: taskViewModel) { (response) in
                if (response?.taskId != nil) {
                    self.fetchTasks()
                } else {
                    self.presentAlertViewController(title: "Error", message: "Error occured while creating the task. Please try again")
                }
            }
        }
    }
    
    
