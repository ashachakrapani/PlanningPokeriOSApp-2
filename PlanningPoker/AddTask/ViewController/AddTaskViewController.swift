//
//  AddTaskViewController.swift
//  PlanningPoker
//
//  Created by Asha Chakrapani on 6/14/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import UIKit

///Enum for each text fields corresponding tag property set in storyboard
enum NewTaskTextFields: Int {
    case title = 1
    case description = 2
}

protocol NewTaskDelegate: class {
    func save(newTaskViewModel taskViewModel: NewTaskViewModel)
}

class AddTaskViewController: UIViewController {
    
    //MARK: - UIViewController Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNavigationBar()
        self.addBorderToTextView()
        //self.taskTitleTextField.delegate = self
    }
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var addNewTaskTitleLabel: UILabel!
    @IBOutlet weak var taskTitleLabel: UILabel!
    @IBOutlet weak var taskTitleTextField: UITextField!
    @IBOutlet weak var taskDescriptionTitleLabel: UILabel!
    @IBOutlet weak var taskDescriptionTextView: UITextView!
    
    //MARK: - Private API
    
    weak var delegate: NewTaskDelegate?
    private var newTaskViewModel = NewTaskViewModel(taskTitle: nil, taskDescription: nil)
    
    private func setUpNavigationBar() {
        //self.taskDescriptionTextView.delegate = self
        let addTaskNavBarButton = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveTaskButtonTouchUp(sender:)))
        self.navigationItem.rightBarButtonItem = addTaskNavBarButton
    }
    
    @objc private func saveTaskButtonTouchUp(sender: UIBarButtonItem) {
        self.updateViewModel()
        if let taskDescription = self.newTaskViewModel.taskDescription {
            if (taskDescription.isEmpty) {
                self.presentAlertViewController(title: "Missing Information", message: "Enter task description to save.")
                return
            }
        }
        self.delegate?.save(newTaskViewModel: self.newTaskViewModel)
        self.navigationController?.popViewController(animated: false)
    }
    
    private func updateViewModel() {
        if let _text = self.taskTitleTextField.text, let fieldTag = NewTaskTextFields(rawValue: self.taskTitleTextField.tag) {
            self.addToNewTaskViewModel(tag: fieldTag, text: _text)
        }
        if let _text = self.taskDescriptionTextView.text, let fieldTag = NewTaskTextFields(rawValue: self.taskDescriptionTextView.tag) {
            self.addToNewTaskViewModel(tag: fieldTag, text: _text)
        }
    }
    
    private func addToNewTaskViewModel(tag fromField: NewTaskTextFields, text value: String) {
        switch fromField {
        case NewTaskTextFields.title:
            self.newTaskViewModel.taskTitle = value
        case NewTaskTextFields.description:
            self.newTaskViewModel.taskDescription = value
        }
    }
    
    private func addBorderToTextView() {
        self.taskDescriptionTextView.layer.cornerRadius = 5
        self.taskDescriptionTextView.layer.borderColor = UIColor.lightGray.cgColor
        self.taskDescriptionTextView.layer.borderWidth = 0.5
        self.taskDescriptionTextView.layer.masksToBounds = true
        //TODO: refactor to also set the font size of the text view
    }
}
