//
//  NewTaskViewModel.swift
//  PlanningPoker
//
//  Created by Asha Chakrapani on 6/21/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation

public struct NewTaskViewModel: Codable {
    
    ///Gets or sets the task title
    var taskTitle: String?
    
    ///Gets or sets the task description
    var taskDescription: String?

}
