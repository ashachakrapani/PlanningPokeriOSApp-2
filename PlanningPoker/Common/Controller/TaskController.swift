//
//  TaskController.swift
//  PlanningPoker
//
//  Created by Asha Chakrapani on 6/16/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation
import Alamofire
import SocketIO

typealias TaskFetchCompletion = (_ taskFetchResponse: TaskFetchResponse?) -> Void
typealias TaskDeleteCompletion = (_ statusCode: Int?, _ deleteTaskResponse: DeleteTaskResponse?) -> Void
typealias TaskCreateCompletion  = (_ createTaskResponse: CreateTaskResponse?) -> Void
typealias TaskPointEmitEventCompletion = (_ success: Bool?) -> Void
typealias TaskPointsUpdatedEventCompletion = (_ updatedTaskPoints: TaskPointsResponse?) -> Void
typealias PointsDashboardDataFetchCompletion = (_ taskPoints: PointsDashboardResponse?) -> Void

class TaskController {
    
    //TODO: update status codes across all service calls.
    func createNewTask(withDetails newTaskViewModel: NewTaskViewModel, completion: @escaping TaskCreateCompletion) -> Void {
        
        let encoder = JSONEncoder()
        let data = try! encoder.encode(newTaskViewModel)
        print(String(data: data, encoding: .utf8)!)
        
        var createTaskResponse: CreateTaskResponse?
        
        if let url = URL(string: Routes.Task.createTask) {
            var request = URLRequest(url: url)
            request.httpMethod = HTTPMethod.post.rawValue
            request.httpBody = data
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            Alamofire.request(request).responseJSON { (response) in
                if let data = response.data {
                    let decoder = JSONDecoder()
                    createTaskResponse = try? decoder.decode(CreateTaskResponse.self, from: data)
                }
                completion(createTaskResponse)
            }
        }
        
    }
    
    func fetchTasks(completion: @escaping TaskFetchCompletion) -> Void {
        var taskFetchResponse: TaskFetchResponse? = nil
        Alamofire.request(Routes.Task.tasks).responseJSON { (response) in
            if let data = response.data {
                let decoder = JSONDecoder()
                taskFetchResponse = try? decoder.decode(TaskFetchResponse.self, from: data)
            }
            completion(taskFetchResponse)
        }
    }
    
    func deleteTask(withObjectId objectId: String, completion: @escaping TaskDeleteCompletion) -> Void {
        var deleteTaskResponse: DeleteTaskResponse? = nil
        let params: Parameters = ["id": objectId]
        Alamofire.request(Routes.Task.deleteTask,
                          method: .delete,
                          parameters: params,
                          encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
                            let statusCode = response.response?.statusCode
                            if let data = response.data {
                                let decoder = JSONDecoder()
                                deleteTaskResponse = try? decoder.decode(DeleteTaskResponse.self, from: data)
                            }
                            completion(statusCode, deleteTaskResponse)
        }
    }
    
    func fetchPointsDashboardData(forTaskId taskId: Int, completion: @escaping PointsDashboardDataFetchCompletion) -> Void {
        var urlComponents = URLComponents(string: Routes.Task.taskDetails)
        urlComponents?.queryItems = [URLQueryItem(name: "taskId", value: String(taskId))]
        
        
        var pointsDashboardResponse: PointsDashboardResponse? = nil
        Alamofire.request(urlComponents!).responseJSON { (response) in
            if let data = response.data {
                let decoder = JSONDecoder()
                pointsDashboardResponse = try? decoder.decode(PointsDashboardResponse.self, from: data)
            }
            completion(pointsDashboardResponse)
        }
    }
    
    
    func emitTaskPointUpdateEvent(objectId _id: String, forTaskId taskId: Int, withPoints points: Int, byMemberId memberId: Int, completion: @escaping TaskPointEmitEventCompletion) -> Void {
        SocketController.sharedInstance.socket?.emit("TaskPointsUpdateRequiredEvent", _id, taskId, points, memberId)
        completion(true)
    }
    
    func listenToTaskPointUpdatedEvent(completion: @escaping TaskPointsUpdatedEventCompletion) -> Void {
        SocketController.sharedInstance.socket?.on("TaskPointsSummaryEvent") { (dataArray, ack) in
            _ = dataArray.map({$0 as? String})
            let objectId = dataArray[0] as? String ?? ""
            let taskId = dataArray[1] as? Int ?? -1
            let taskPoint = dataArray[2] as? Int ?? -1
            let memberId = dataArray[3] as? Int ?? -1

            let updatedTask: TaskPointsResponse = TaskPointsResponse(taskId: taskId, _id: objectId, taskPoint: taskPoint, memberId: memberId)
            completion(updatedTask)
        }
    }
}
