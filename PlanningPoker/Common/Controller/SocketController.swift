//
//  SocketController.swift
//  PlanningPoker
//
//  Created by Asha Chakrapani on 6/19/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation
import SocketIO

class SocketController {
    
    //MARK: - Singleton Implementation
    
    static var sharedInstance = SocketController()
    
    //MARK: - Private API
    
    private init() {
        //Implicitly wrapping because it is a static string
        self.socketManager = SocketManager(socketURL: URL(string: Routes.Socket.socketURL)!, config: [.log(true), .compress])
        self.socket = socketManager?.defaultSocket
    }
    
    var socket: SocketIOClient?
    var socketManager: SocketManager?
    
    //MARK: - Public API
    func establishConnection() {
        self.socket?.connect()
    }
    
    func closeConnection() {
        self.socket?.disconnect()
    }
}
