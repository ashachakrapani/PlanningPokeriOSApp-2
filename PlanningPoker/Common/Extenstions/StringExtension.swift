//
//  StringExtension.swift
//  PlanningPoker
//
//  Created by Asha Chakrapani on 6/18/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation

public extension String {
    func concat(string2: String?, withSeparator separator: String?) -> String {
        let str1 = self
        let str2 = string2 ?? ""
        let sep = separator ?? ""
        return ((str1.isEmpty) ? "" : ((str2.isEmpty) ? str1 : str1 + sep)) + str2
    }
}
