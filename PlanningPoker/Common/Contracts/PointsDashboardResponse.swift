//
//  TaskSummaryResponse.swift
//  PlanningPoker
//
//  Created by Asha Chakrapani on 6/21/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation

struct PointsDashboardResponse : Codable {
    let items: [TaskPointsResponse]?
}
