//
//  TaskPointsUpdate.swift
//  PlanningPoker
//
//  Created by Asha Chakrapani on 6/19/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation

struct TaskPointsResponse: Codable {
    let taskId: Int?
    let _id: String?
    var taskPoint: Int?
    let memberId: Int?
}
