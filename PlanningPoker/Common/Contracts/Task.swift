//
//  Task.swift
//  PlanningPoker
//
//  Created by Asha Chakrapani on 6/15/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation

public struct Task: Codable {
    
    ///Gets or sets the mongodb object id of the task
    let objectId: String?
    
    ///Gets or sets the tsk id
    let taskId: Int?
    
    ///Gets or sets the taskTitle
    let taskTitle: String?
    
    ///Gets or sets the task description
    let taskDescription: String?
    
    ///Gets or sets the points
    let points: FibonacciLevel?
}
