//
//  CreateTaskResponse.swift
//  PlanningPoker
//
//  Created by Asha Chakrapani on 6/24/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation

public struct CreateTaskResponse: Codable {
    
    ///Gets or sets the task id of the newly created task
    let taskId: Int?
}
