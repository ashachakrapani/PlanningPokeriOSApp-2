//
//  TaskDeleteResponse.swift
//  PlanningPoker
//
//  Created by Asha Chakrapani on 8/19/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation

public struct DeleteTaskResponse: Codable {
    
    ///Gets or sets the mongodb object id of the task
    let objectId: String?
}
