//
//  TaskList.swift
//  PlanningPoker
//
//  Created by Asha Chakrapani on 6/15/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation

public struct TaskFetchResponse: Codable {
    
    ///Gets or sets the tadks from the response
    let tasks: [Task]?
}
