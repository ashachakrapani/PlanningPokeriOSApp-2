//
//  FibonacciLevel.swift
//  PlanningPoker
//
//  Created by Asha Chakrapani on 6/15/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation

public enum FibonacciLevel: Int, Codable {
    case zero = 0
    case one = 1
    case two = 2
    case three = 3
    case five = 5
    case eight = 8
    case thirteen = 13
}

extension FibonacciLevel {
    static var allAvailablePoints: [FibonacciLevel] {
        var pointsArray: [FibonacciLevel] = []
        switch FibonacciLevel.zero {
        case .zero: pointsArray.append(.zero)
            fallthrough
        case .one:pointsArray.append(.one)
            fallthrough
        case .two: pointsArray.append(.two)
            fallthrough
        case .three: pointsArray.append(.three)
            fallthrough
        case .five: pointsArray.append(.five)
            fallthrough
        case .eight: pointsArray.append(.eight)
            fallthrough
        case .thirteen: pointsArray.append(.thirteen)
        }
        return pointsArray
    }
}
