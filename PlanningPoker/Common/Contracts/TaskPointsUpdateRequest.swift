//
//  TaskPointsUpdateRequest.swift
//  PlanningPoker
//
//  Created by Asha Chakrapani on 6/19/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation

struct TaskPointsUpdateRequest {
    let taskId: Int
    let points: Int
}
