//
//  Routes.swift
//  PlanningPoker
//
//  Created by Asha Chakrapani on 6/18/18.
//  Copyright © 2018 Asha Chakrapani. All rights reserved.
//

import Foundation

struct Routes {
    
    //Host URL - In case of real apps, we should take into considerations the various environments.
    static let baseURL = "https://planningpoker-api.herokuapp.com/api/"
    static let baseLocalhostURL = "http://localhost:5000/api/"
    
    struct Socket {
        ///All tasks available
        static let socketURL = "\(baseURL)tasks"
    }
    
    struct Task {
        ///All tasks available
        static let tasks = "\(baseURL)tasks"
        
        ///Task details for the give task id
        static let taskDetails = "\(baseURL)tasks/taskdetail"
        
        ///Update task points
        static let updateTask = "\(baseURL)tasks/{taskId}"

        ///Creates new tasks
        static let createTask = "\(baseURL)task"
        
        ///Delete a task
        static let deleteTask = "\(baseURL)tasks"
    }
}
