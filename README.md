# PlanningPokeriOSApp

App Features:

The app allows a person to view a list of available tasks or add a new one to the existing list. 
A random memder id is required to enter into the app and serves as a unique identifier against which the points are stored. Any particular task can be scored a point. 
If any other user is scoring the same task at the same time, the 'Points Dashboard' on the TaskDetail screen displays the points and a count of the number of people who have scored the corresponding points. 
Any given task's description can be shared using the share feature on the task detail page. Although memberId scoring s specific point is stored in the Database, it is currently not displayed in the app.

App is unit tested on iPhone 8 simulator and iPhone SE device. Although it is still under testing, all the above features work on my machine! :)

To run the project -

1. Clone project using the url from the repository git clone https://github.com/ashachakrapani/PlanningPokeriOSApp.git
2. cd into the directory containing the Podfile and run pod install to install the pods required for the project.
3. Locate the .workspace file from the downloaded repository and open it

The project's backend is based on Node.js, Express and MongoDB hosted in Heroku. By default the urls are wired up to point to the app running on Heroku. 

The backend project can be run on localhost to run the app locally. To do so, replace the baseURL with baseLocalhostURL for all routes in the PlanningPokeriOSApp/PlanningPoker/Common/Routing/Routes.swift file in the iOS app. To host and run the project's backend locally, follow the instructions in the README.md of the repository https://github.com/ashachakrapani/planning-poker-app.git
